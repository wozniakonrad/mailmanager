﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Model
{
    public class MailMessage
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public int? SenderId { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; } 

        public User Sender { get; set; }
        public ICollection<MailRecipient> MailRecipients { get; set; }
    }
}
