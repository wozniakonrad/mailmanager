﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Model
{
    public class MessagePriority
    {
        public static string Normal = "Normal";
        public static string Low = "Low";
        public static string High = "High";
    }
}
