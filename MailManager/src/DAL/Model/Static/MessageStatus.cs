﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Model
{
    public static class MessageStatus
    {
        public static string Sent = "Sent";
        public static string Pending = "Pending";
        public static string Failed = "Failed";
    }
}
