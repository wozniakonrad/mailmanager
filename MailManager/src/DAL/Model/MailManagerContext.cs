﻿using JetBrains.Annotations;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace DAL.Model
{
    public class MailManagerContext : DbContext
    {
        public MailManagerContext()
        { }

        public MailManagerContext(DbContextOptions<MailManagerContext> options)
            : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<MailMessage> MailMessages { get; set; }
        public DbSet<MailRecipient> MailRecipients { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = "MailManager.db" };
                var connectionString = connectionStringBuilder.ToString();
                var connection = new SqliteConnection(connectionString);

                optionsBuilder.UseSqlite(connection);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MailRecipient>()
                .HasKey(mr => new { mr.UserId, mr.MailMessageId });
            modelBuilder.Entity<MailRecipient>()
                .HasOne(arg => arg.MailMessage)
                .WithMany(arg => arg.MailRecipients)
                .HasForeignKey(arg => arg.MailMessageId);
            //modelBuilder.Entity<MailRecipient>()
            //    .HasOne(arg => arg.User)
            //    .WithMany(arg => arg.MailRecipients)
            //    .HasForeignKey(arg => arg.UserId);

            //scofolding
            modelBuilder.Entity<User>().HasData(
               new User { Id = 1, FirstName = "User1", LastName = "User1", EmailAddress = "user1@gmail.com" },
               new User { Id = 2, FirstName = "User2", LastName = "User2", EmailAddress = "user2@gmail.com" },
               new User { Id = 3, FirstName = "User3", LastName = "User3", EmailAddress = "user3@gmail.com" },
               new User { Id = 4, FirstName = "User4", LastName = "User4", EmailAddress = "user4@gmail.com" }
           );

            modelBuilder.Entity<MailMessage>().HasData(
               new MailMessage { Id = 1, SenderId = 3, Subject = "First message", Status = MessageStatus.Pending, Message = "Body of the message" }
           );

            modelBuilder.Entity<MailRecipient>().HasData(
              new MailRecipient { MailMessageId = 1, UserId = 1 },
              new MailRecipient { MailMessageId = 1, UserId = 2 }
          );
        }
    }
}
