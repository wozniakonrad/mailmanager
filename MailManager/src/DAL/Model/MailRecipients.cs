﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Model
{
    public class MailRecipient
    {
        //public int Id { get; set; }
        public int MailMessageId { get; set; }
        public int UserId { get; set; }

        public MailMessage MailMessage { get; set; }
        public User User { get; set; }
    }
}
