﻿using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
    public interface IMailMessageService
    {
        IQueryable<MailMessage> GetList();
        MailMessage Create(MailMessage model, List<User> recipients);
        MailMessage GetById(int id);
        void UpdateMessageStatus(int mailMessageId, string status);
    }
}
