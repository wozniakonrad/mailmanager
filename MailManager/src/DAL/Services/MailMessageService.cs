﻿using DAL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Services
{
    public class MailMessageService : IMailMessageService
    {
        MailManagerContext _context;

        public MailMessageService(MailManagerContext context)
        {
            _context = context;
        }

        public IQueryable<MailMessage> GetList()
        {
            return _context.MailMessages
                .Include(arg => arg.Sender)
                .Include(arg => arg.MailRecipients)
                .Include("MailRecipients.User");
        }

        // sqlite needs to operate on id while creating record in MailRecipient. That's why more than one SaveChanges.
        public MailMessage Create(MailMessage model, List<User> recipients)
        {
            model.Status = MessageStatus.Pending;
            if (model.Sender != null && model.Sender.Id == 0)
            {
                var senderDb = _context.Users.FirstOrDefault(arg => arg.EmailAddress.ToLower() == model.Sender.EmailAddress.ToLower());
                if (senderDb == null)
                    _context.Users.Add(model.Sender);
                else
                    model.Sender = senderDb;
            }
            _context.MailMessages.Add(model);
            _context.SaveChanges();

            ManageRecipients(model, recipients);

            return GetById(model.Id);
        }

        

        public void UpdateMessageStatus(int mailMessageId, string status)
        {
            var message = _context.MailMessages.FirstOrDefault(arg => arg.Id == mailMessageId);
            message.Status = status;
            _context.MailMessages.Update(message);
            _context.SaveChanges();
        }

        public MailMessage GetById(int id)
        {
            return _context.MailMessages
                .Include(arg => arg.Sender)
                .Include(arg => arg.MailRecipients)
                .Include("MailRecipients.User")
                .FirstOrDefault(arg => arg.Id == id);
        }

        private void ManageRecipients(MailMessage model, List<User> recipients)
        {
            for (int i = 0; i < recipients?.Count(); i++)
            {
                var recipient = recipients[i];
                // new item. need to be add to db 
                if (recipient.Id == 0)
                {
                    // check again if the email is not in db
                    var recipientDb = _context.Users.FirstOrDefault(arg => arg.EmailAddress.ToLower() == recipient.EmailAddress.ToLower());
                    if (recipientDb == null) // everything ok, add new item
                    {
                        _context.Users.Add(recipient);
                        _context.SaveChanges();
                    }
                    else
                    {
                        recipient = recipientDb;
                    }
                }
                MailRecipient mr = new MailRecipient()
                {
                    MailMessageId = model.Id,
                    UserId = recipient.Id
                };
                _context.MailRecipients.Add(mr);
            }


            _context.SaveChanges();
        }
    }
}
