﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MailManager.ViewModels
{
    public class ErrorViewModel
    {
        public string Error { get; set; }

        public Dictionary<string, string[]> ErrorMessages { get; set; }

        public ErrorViewModel()
        {
            ErrorMessages = new Dictionary<string, string[]>();
        }
    }
}
