﻿using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MailManager.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }

        public static UserViewModel ConvertToViewModel(User model)
        {
            return new UserViewModel()
            {
                Id = model.Id,
                EmailAddress = model.EmailAddress,
                FirstName = model.FirstName,
                LastName = model.LastName
            };
        }

        public static User ConvertToModel(UserViewModel viewModel)
        {
            return new User()
            {
                Id = viewModel.Id,
                EmailAddress = viewModel.EmailAddress,
                FirstName = viewModel.FirstName,
                LastName = viewModel.LastName
            };
        }
    }
}
