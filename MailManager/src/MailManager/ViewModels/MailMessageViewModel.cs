﻿using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MailManager.ViewModels
{
    public class MailMessageViewModel
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public int? SenderId { get; set; }
        public string Priority { get; set; }

        public UserViewModel Sender { get; set; }
        public IEnumerable<UserViewModel> Recipients { get; set; }

        public static MailMessageViewModel ConvertToViewModel(MailMessage model)
        {
            return new MailMessageViewModel()
            {
                Id = model.Id,
                Message = model.Message,
                Sender = model.Sender != null ? UserViewModel.ConvertToViewModel(model.Sender) : null,
                SenderId = model.SenderId,
                Subject = model.Subject,
                Status = model.Status,
                Priority = model.Priority,
                Recipients = model.MailRecipients.Select(arg => UserViewModel.ConvertToViewModel(arg.User)).Distinct()
            };
        }

        public static MailMessage ConvertToModel(MailMessageViewModel viewModel)
        {
            return new MailMessage()
            {
                Id = viewModel.Id,
                Message = viewModel.Message,
                Sender = viewModel.Sender != null ? UserViewModel.ConvertToModel(viewModel.Sender) : null,
                SenderId = viewModel.Sender?.Id,
                Subject = viewModel.Subject,
                Priority = viewModel.Priority,
                Status = viewModel.Status
            };
        }
    }
}
