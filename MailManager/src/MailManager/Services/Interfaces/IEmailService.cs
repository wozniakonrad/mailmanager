﻿using MailManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentEmail.Core.Models;
using System.Threading.Tasks;

namespace MailManager.Services
{
    public interface IEmailService
    {
        Task<bool> SendEmailAsync(string email, string subject, string message);
        Task<bool> SendEmailAsync(MailMessageViewModel model, string attachmentFilePath = "");
        Task<bool> SendEmailAsync(MailMessageViewModel model, List<Attachment> attachments);
    }
}
