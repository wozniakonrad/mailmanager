﻿using DAL.Model;
using FluentEmail.Core;
using FluentEmail.Core.Models;
using MailManager.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MailManager.Services
{
    public class EmailService : IEmailService
    {
        private readonly IFluentEmail _email;

        public EmailService([FromServices]IFluentEmail email)
        {
            _email = email;
        }

        public async Task<bool> SendEmailAsync(string email, string subject, string message)
        {
            _email
              .To(email)
                  .Subject(subject)
                  .Body(message);

            SendResponse response = await _email.SendAsync();
            if (response.Successful)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> SendEmailAsync(MailMessageViewModel model, string attachmentFilePath = "")
        {
            try
            {
                var recipients = model.Recipients.Select(arg => new Address(arg.EmailAddress)).ToList();

                _email
                  .To(recipients)
                  .Subject(model.Subject)
                  .Body(model.Message);

                if (model.Sender != null)
                {
                    _email.SetFrom(model.Sender.EmailAddress);
                }

                if (!string.IsNullOrEmpty(attachmentFilePath))
                {
                    _email.AttachFromFilename(attachmentFilePath);
                }

                if (model.Priority == MessagePriority.Low)
                    _email.LowPriority();
                if (model.Priority == MessagePriority.High)
                    _email.HighPriority();

                SendResponse response = await _email.SendAsync();
                return response.Successful;
            }
            catch(Exception ex)
            {
                // log and handle exception
                return false;
            }
        }

        public async Task<bool> SendEmailAsync(MailMessageViewModel model, List<Attachment> attachments = null)
        {
            var recipients = model.Recipients.Select(arg => new Address(arg.EmailAddress)).ToList();

            _email
              .To(recipients)
              .Subject(model.Subject)
              .Body(model.Message);

            if (model.Sender != null)
            {
                _email.SetFrom(model.Sender.EmailAddress);
            }

            if (attachments.Count > 0)
            {
                _email.Attach(attachments);
            }

            if (model.Priority == MessagePriority.Low)
                _email.LowPriority();
            if (model.Priority == MessagePriority.High)
                _email.HighPriority();

            SendResponse response = await _email.SendAsync();
            return response.Successful;
        }

    }
}
