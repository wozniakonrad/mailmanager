using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Model;
using MailManager.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Net;
using System.Net.Mail;
using DAL.Services;
using MailManager.Services;

namespace MailManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            using (var db = new MailManagerContext())
            {
                db.Database.EnsureCreated();
                db.Database.Migrate();
            }
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            services.AddDbContext<MailManagerContext>();

            var appSettings = appSettingsSection.Get<AppSettings>();
            ConfigureMailService(services, appSettings);

            services.AddScoped<IMailMessageService, MailMessageService>();
            services.AddScoped<IEmailService, EmailService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void ConfigureMailService(IServiceCollection services, AppSettings appSettings)
        {
            string login = appSettings.EmailProvider.Login;
            string password = appSettings.EmailProvider.Password;
            string smtpServer = appSettings.EmailProvider.SmtpServer;
            int smtpPort = appSettings.EmailProvider.SmtpPort;
            bool smtpEnableSsl = appSettings.EmailProvider.SmtpEnableSsl;

            SmtpClient mailClient = new SmtpClient(smtpServer, smtpPort)
            {
                EnableSsl = smtpEnableSsl,
                Credentials = new NetworkCredential(login, password)
            };

            services
                .AddFluentEmail(login)
                //.AddRazorRenderer()
                .AddSmtpSender(mailClient);
        }
    }
}
