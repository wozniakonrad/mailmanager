﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Model;
//using DAL.Model;
using DAL.Services;
using MailManager.Services;
using MailManager.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MailManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {
        IMailMessageService _mailMessageService;
        IEmailService _emailService;

        public MailController(IMailMessageService mailMessageService, IEmailService emailService)
        {
            _mailMessageService = mailMessageService;
            _emailService = emailService;
        }

        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            IEnumerable<MailMessageViewModel> result = null;
            await Task.Run(() =>
            {
                var mailMessagesDb = _mailMessageService.GetList();
                result = mailMessagesDb?.Select(arg => MailMessageViewModel.ConvertToViewModel(arg)).AsEnumerable();
            });

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            MailMessageViewModel result = null;
            await Task.Run(() =>
            {
                var mailMessageDb = _mailMessageService.GetById(id);
                result = MailMessageViewModel.ConvertToViewModel(mailMessageDb);
            });

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]MailMessageViewModel model)
        {
            MailMessageViewModel result = null;

            ErrorViewModel validationErrors = new ErrorViewModel() { Error = "validation_failed" };
            ValidateMailMessage(model, validationErrors);
            if(validationErrors.ErrorMessages.Count > 0)
            {
                return BadRequest(validationErrors);        // ========>
            }

            await Task.Run(() =>
            {
                var mailModel = MailMessageViewModel.ConvertToModel(model);
                var recipients = model.Recipients.Select(arg => UserViewModel.ConvertToModel(arg)).ToList();

                var resultModel = _mailMessageService.Create(mailModel, recipients);
                result = MailMessageViewModel.ConvertToViewModel(resultModel);
            });
            return Ok(result);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> SendAllPendings()
        {
            await Task.Run(() =>
            {
                var mailMessagesDb = _mailMessageService.GetList()
                .Where(arg => arg.Status == MessageStatus.Pending);
                var mailMessagesVm = mailMessagesDb?.Select(arg => MailMessageViewModel.ConvertToViewModel(arg)).AsEnumerable();

                foreach (var item in mailMessagesVm)
                {
                    bool result = _emailService.SendEmailAsync(item).Result;
                    if (result)
                        item.Status = MessageStatus.Sent;
                    else
                        item.Status = MessageStatus.Failed;

                    _mailMessageService.UpdateMessageStatus(item.Id, item.Status);
                }

                // should be one call to update pending mail statuses with collection of mail message
            });

            return Ok();
        }

        #region helpers
        private void ValidateMailMessage(MailMessageViewModel model, ErrorViewModel validationErrors)
        {
            if(model.Recipients.Count() == 0)
            {
                validationErrors.ErrorMessages.Add(nameof(model.Recipients), new string[] { "required_parameter" });
            }
            // .....
            // more validation here

        }
        #endregion
    }
}