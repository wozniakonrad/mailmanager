﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MailManager.Helpers
{
    public class AppSettings
    {
        public EmailProvider EmailProvider { get; set; }
    }

    public class EmailProvider
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public bool SmtpEnableSsl { get; set; }
        public string Host { get; set; }
    }
}
