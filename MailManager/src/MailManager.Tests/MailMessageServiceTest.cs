﻿using DAL.Model;
using DAL.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MailManager.Tests
{
    public class MailMessageServiceTest
    {
        [Fact]
        public void GetById_KnownIdPassed_ReturnsNotNull()
        {
            var options = new DbContextOptionsBuilder<MailManagerContext>().UseInMemoryDatabase(databaseName: "in_memory").Options;

            // arrange
            using (var context = new MailManagerContext(options))
            {
                context.Set<MailMessage>().Add(new MailMessage() { Message = "First message" });
                context.SaveChanges();
            }

            using (var context = new MailManagerContext(options))
            {
                // create the service
                var service = new MailMessageService(context);

                // act
                var result = service.GetById(2);

                // assert
                Assert.NotNull(result);
                Assert.Equal(2, result.Id);
                Assert.Equal("First message", result.Message);
            }
        }

        [Fact]
        public void GetById_UnknownIdPassed_ReturnsNull()
        {
            var options = new DbContextOptionsBuilder<MailManagerContext>().UseInMemoryDatabase(databaseName: "in_memory").Options;

            // arrange
            using (var context = new MailManagerContext(options))
            {
                context.Set<MailMessage>().Add(new MailMessage() { Message = "First message" });
                //context.MailMessages.Add(new MailMessage() { Id = 1, Message= "First message" });
                context.SaveChanges();
            }

            using (var context = new MailManagerContext(options))
            {
                // create the service
                var service = new MailMessageService(context);

                // act
                var result = service.GetById(5);

                // assert
                Assert.Null(result);
            }
        }

        [Fact]
        public void Create_ValidObject_ReturnsNewObject()
        {
            var options = new DbContextOptionsBuilder<MailManagerContext>().UseInMemoryDatabase(databaseName: "in_memory").Options;

            // arrange
            MailMessage mailMessage = new MailMessage()
            {
                Message = "message",
                Subject = "subject",
                Sender = new User() { EmailAddress = "a@o2.pl" }
            };

            using (var context = new MailManagerContext(options))
            {
                // create the service
                var service = new MailMessageService(context);

                // act
                var result = service.Create(mailMessage, null);

                // assert
                Assert.NotNull(result);
                Assert.Equal(0, result.MailRecipients.Count);
            }
        }

        [Fact]
        public void Create_ValidObjectWithRecipients_ReturnsNewObject()
        {
            var options = new DbContextOptionsBuilder<MailManagerContext>().UseInMemoryDatabase(databaseName: "in_memory").Options;

            // arrange
            MailMessage mailMessage = new MailMessage()
            {
                Message = "message",
                Subject = "subject",
                Sender = new User() { Id = 2, EmailAddress = "a@o2.pl" }
            };

            List<User> recipients = new List<User>();
            recipients.Add(new User() { EmailAddress = "a@o2.pl" });
            recipients.Add(new User() { EmailAddress = "b@o2.pl" });

            using (var context = new MailManagerContext(options))
            {
                // create the service
                var service = new MailMessageService(context);

                // act
                var result = service.Create(mailMessage, recipients);

                // assert
                Assert.NotNull(result);
                Assert.Equal(2, result.MailRecipients.Count);
            }
        }
    }
}
